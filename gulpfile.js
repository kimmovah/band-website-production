'use strict';

var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins'),
  plugins = gulpLoadPlugins();

var jssrc = 'wsgi/band_webpage/static/scripts/*.js';

// Bower components to copy over
var libsrc = [
  'bower_components/jquery/dist/jquery.min.js',
  'bower_components/jquery-backstretch/jquery.backstretch.min.js',
  'bower_components/foundation/js/foundation.min.js',
  'bower_components/foundation/js/foundation/foundation.clearing.js',
  'bower_components/modernizr/modernizr.js'
];

// Copies needed scripts from bower over to the scripts directory
gulp.task('libcopy', function () {
    gulp.src(libsrc)
    .pipe(gulp.dest('wsgi/band_webpage/static/scripts'));
});

gulp.task('jshint', function () {
  gulp.src('wsgi/band_webpage/static/scripts/site-main.js')
    .pipe(plugins.jshint())
    .pipe(plugins.jshint.reporter('default'))
});

gulp.task('concat', function () {
  gulp.src(jssrc)
    .pipe(plugins.concat('app.js'))
    .pipe(gulp.dest('build'));
});

gulp.task('minjs', function () {
  gulp.src(jssrc)
    .pipe(plugins.uglify())
    .pipe(plugins.rename(function (path) {
      path.dirname += '/min';
      path.basename += '.min';
    }))
    .pipe(gulp.dest('wsgi/band_webpage/static/scripts'))
});

gulp.task('js', ['jshint', 'concat', 'minjs']);

gulp.task('sass', function () {
  gulp.src('scss/style_main.scss')
    .pipe(plugins.sass({includePaths: ['bower_components/foundation/scss']}).on('error', plugins.sass.logError))
    .pipe(plugins.autoprefixer())
    .pipe(gulp.dest('wsgi/band_webpage/static/styles'));
});

gulp.task('mincss', function () {
  gulp.src('wsgi/band_webpage/static/styles/*.css')
    .pipe(plugins.minifyCss())
    .pipe(gulp.dest('wsgi/band_webpage/static/styles'));
});

gulp.task('watch', function () {
  gulp.watch('scss/*.scss', ['sass']);
});

gulp.task('build', ['libcopy', 'sass']);

// define the default task and add the watch task to it
gulp.task('default', ['watch']);
