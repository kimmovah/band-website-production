###To compile the *.po and *.mo files needed for localization:

(Requires GNU gettext. For more information, see [this page](https://docs.djangoproject.com/en/1.9/topics/i18n/translation/) from the Django docs.)

1. Edit the English texts as needed.
2. Activate your virtualenv for this project (if not already done).
3. From the `band-website/wsgi/band_webpage` directory, run `python ../manage.py makemessages -l fi` to generate the *.po file for Finnish.
4. Edit the Finnish *.po file as needed.
5. Run `python ../manage.py compilemessages` to generate the *.mo file for Finnish.
6. Remember to restart the dev server to see localization changes reflected in the browser.
