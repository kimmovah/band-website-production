#!/usr/bin/env python
# -- coding: utf-8 -- 

from setuptools import setup

setup(name='bandwebsite',
      version='1.0',
      description='Website for band Ääriviivat',
      author='Justin Goney',
      author_email='goulash@gmail.com',
      url='http://www.python.org/sigs/distutils-sig/',
      )
