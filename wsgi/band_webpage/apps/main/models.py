from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Song(models.Model):
    title = models.CharField(max_length=255, primary_key=True)
    lyrics = models.TextField()
    url = models.URLField(blank=True)
    slug = models.SlugField()
    credit = models.CharField(max_length=255, blank=True)
    track = models.IntegerField(blank=True, null=True)

    def __unicode__(self):
        return self.title


class Tag(models.Model):
    tag = models.CharField(max_length=255)
    slug = models.SlugField()

    def __unicode__(self):
        return self.tag


class BlogEntry(models.Model):
    title = models.CharField(max_length=255)
    body = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField()
    user = models.ForeignKey(User)
    tags = models.ManyToManyField(Tag)
    spotify_uri = models.CharField(max_length=255, blank=True)

    def __unicode__(self):
        return unicode("%s by %s (%s %s)" % (self.title, self.user, self.date.date(), self.date.time()))


class Band(models.Model):
    name = models.CharField(max_length=255)
    url = models.URLField(blank=True)

    def __unicode__(self):
        return unicode(self.name)


class Show(models.Model):
    venue = models.CharField(max_length=255)
    date = models.DateField()
    time = models.TimeField()
    url = models.URLField(blank=True)
    bands = models.ManyToManyField(Band)

    def __unicode__(self):
        return unicode("%s (%s, %s)" % (self.venue, self.date, self.time.strftime("%H:%M")))
