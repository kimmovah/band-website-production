import datetime

from django.views.generic import TemplateView
from django.shortcuts import redirect
from django.core.urlresolvers import reverse

from models import BlogEntry, Show, Song, Tag


# Using class-based views.
class SiteBaseView(TemplateView):
    def get_context_data(self, **kwargs):
        context = super(SiteBaseView, self).get_context_data(**kwargs)

        start_date = datetime.date.today()
        end_date = start_date + datetime.timedelta(days=7)
        # This should ultimately return a list to be rendered as a list by the template
        shows = Show.objects.filter(date__range=(start_date, end_date))
        if shows:
            context['show'] = shows[0]
            if shows[0].date == datetime.date.today():
                context['show_today'] = True

        return context


class HomePageView(SiteBaseView):
    template_name = "main/home.html"


class AboutPageView(SiteBaseView):
    template_name = "main/about.html"


class ContactPageView(SiteBaseView):
    template_name = "main/contact.html"


class LyricsView(SiteBaseView):
    template_name = "main/lyrics.html"

    def get_context_data(self, **kwargs):
        context = super(LyricsView, self).get_context_data(**kwargs)
        context['all_songs'] = Song.objects.all().order_by('track')
        try:
            context['song'] = Song.objects.get(slug=self.kwargs['song_title'])
        except (Song.DoesNotExist, KeyError):
            context['song'] = context['all_songs'][0]
        return context


class ShowsView(SiteBaseView):
    template_name = "main/shows.html"

    def get_context_data(self, **kwargs):
        context = super(ShowsView, self).get_context_data(**kwargs)
        context['future_shows'] = []
        context['past_shows'] = []

        for show in Show.objects.all().order_by('-date'):
            if show.date >= datetime.date.today():
                context['future_shows'].append(show)
            elif show.date < datetime.date.today():
                context['past_shows'].append(show)

        return context


class BlogBaseView(SiteBaseView):
    def get_context_data(self, **kwargs):
        context = super(BlogBaseView, self).get_context_data(**kwargs)
        context['all_entries'] = BlogEntry.objects.all().order_by('-date')  # TODO: limit this to 10 or so entries
        context['all_tags'] = Tag.objects.all()
        return context


class BlogEntryView(BlogBaseView):
    template_name = "main/blog.html"

    def get_context_data(self, **kwargs):
        context = super(BlogEntryView, self).get_context_data(**kwargs)
        try:
            context['this_entry'] = BlogEntry.objects.get(slug=self.kwargs['blog_title'])
        except (BlogEntry.DoesNotExist, KeyError):
            context['this_entry'] = context['all_entries'][0]

        return context


class BlogTagsView(BlogBaseView):
    template_name = "main/blog_tags.html"

    def dispatch(self, request, *args, **kwargs):
        # check if there is some video onsite
        try:
            self.this_tag = Tag.objects.get(slug=self.kwargs['tag'])
        except (Tag.DoesNotExist, KeyError):
            return redirect(reverse('blog'))

        return super(BlogTagsView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(BlogTagsView, self).get_context_data(**kwargs)
        context['tagged_entries'] = self.this_tag.blogentry_set.all().order_by('-date')
        context['this_tag'] = self.this_tag
        return context
