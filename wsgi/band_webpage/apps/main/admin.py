from django.contrib import admin
from models import Band, BlogEntry, Show, Song, Tag


# Register your models here.
class BlogEntryAdmin(admin.ModelAdmin):
    exclude = ('user',)
    prepopulated_fields = {"slug": ("title",)}

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        obj.save()


class SongAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}


class TagAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("tag",)}


admin.site.register(Band)
admin.site.register(BlogEntry, BlogEntryAdmin)
admin.site.register(Show)
admin.site.register(Song, SongAdmin)
admin.site.register(Tag, TagAdmin)