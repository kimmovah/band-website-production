��    3      �  G   L      h  `
  i  J   �  l       �     �     �  9   �  5   �               .     6     >     R  �   Z  �   ?  n        s     �     �     �  �   �     w     }  
   �     �     �     �     �  Q   �  	   	       �   "  �   �     >     C  	   K     U      ^          �      �     �     �  	   �  	   �     �  	   �     �     �  K  �  �  8  O   -!  l  }!     �"     �"     �"  ?   �"  D   9#     ~#     �#     �#     �#     �#     �#  �   �#  �   i$  ]   %     i%     {%     �%     �%  �   �%     _&     e&     u&  	   �&     �&     �&     �&  Q   �&     '     '  v   '  v   �'     (     (     (  
   ((  "   3(     V(     _(     e(     �(     �(     �(     �(     �(     �(  	   �(     �(                +             ,                  &   (         $   .   	          )         3                 
   !              '                 0      %             #   /             -                    2          1          "       *                    
                    <p>With their insistent hooks, strong melodies, and clever lyrics, Helsinki’s Ääriviivat delivers an
                        interesting marriage of 90’s rock and Suomipop. Their workmanship and genuine love for music can
                        be both heard on the record as well as seen in their live show.</p>
                    <p>The band got its start when workmates Mika Kaukoranta (guitar) and Justin Goney (drums) learned
                        of one-another’s love of all things 90’s rock. The pair had been jamming off and on for a while,
                        but it wasn’t until they met vocalist Iitu Marjosalmi that Ääriviivat really took off. “I was
                        pretty happy just playing Pearl Jam covers,” says Mika. “When I heard Iitu, I just thought
                        ‘man…her voice and energy really takes it to another level.’”</p>
                    <p>Wanting to integrate more modern influences into their music, the group asked Justin’s friend
                        Jane Koskimäki to come in on keyboards, and the current lineup was rounded out with Kimmo
                        Vahteri on bass.</p>
                    <p>Though Ääriviivat’s potent blend of pop and rock is rooted in 90’s grunge/alternative, it also
                        borrows liberally from artists from other genres. Especially close to the band are Finnish
                        artists such as PMMP, Anna Puu, and Maija Vilkkumaa. “I know it sounds really cliché,” starts
                        Justin, “but we just love great music, period. If a song has a great groove, it connects
                        emotionally, or it has outstanding lyrics…we’re into it. We just try to play music that we would
                        enjoy listening to ourselves.”</p>
                    <p>Ääriviivat released their debut album “Kosto” in the summer of 2016. Composed both of hardcore
                        rockers and wistful ballads written in collaboration with the whole band, the album presents an
                        excellent cross-section of the band so far. Iitu says, "We are very proud of our first album."
                        To date, the band has been mostly playing the Helsinki club circuit, but the band is working on
                        expanding its fan base and playing shows outside the capital area.</p>
                    <p>Find Ääriviivat on <a target="_blank" href="https://www.facebook.com/aariviivat">Facebook</a> for
                        up to date news about shows and new music.</p>
                 
                %(title)s (by %(user)s, %(date)s @ %(time)s)
             
                <p>Helsinki's Ääriviivat has been playing smart, tough, and emotional pop/rock since 2013. If you like
                    bands like PMMP, Weezer, and Maija Vilkkumaa, you'll probably like Ääriviivat. If you like bands like
                    Nickelback, you probably won't.</p>
                <p>Pop. Voima. Rock. Rakkaus.</p>
             About About us About Ääriviivat Attention! Come see us on %(date)s @ %(venue)s, %(time)s! Attention! Come see us tonight @ %(venue)s, %(time)s! Blog Booking Ääriviivat Contact English Entries tagged with Finnish For booking information, contact us at
                <a href="mailto:band@aariviivat.fi">band@aariviivat.fi</a>, or connect with us on
                <a target="_blank" href="https://www.facebook.com/aariviivat">Facebook</a>. For booking information, contact us at <a href="mailto:band@aariviivat.fi">band@aariviivat.fi</a>, or connect with us on <a target="_blank" href="https://www.facebook.com/aariviivat">Facebook</a>. For press enquiries, contact us at
                <a href="mailto:band@aariviivat.fi">band@aariviivat.fi</a>. Lyrics: %(credit)s Members Menu dropdown icon Music No upcoming shows for now, but we'll be hitting the road soon! Be sure to like us on <a target="_blank" href="https://www.facebook.com/aariviivat">Facebook</a> to stay up to date with new show announcements! Oops! Page not found Past shows Press Recent entries Shows Tags The page you're looking for can't be found, but we won't let that stop the music! The songs Upcoming shows Whether you're a booking agent at a venue, or a band looking to share a gig, we're always
                interested in new places to play our music. Whether you're a booking agent at a venue, or a band looking to share a gig, we're always interested in new places to play our music. With ^about/ ^contact/ ^music/$ ^music/(?P<song_title>[\w\d-]+)/ ^shows/ bass by %(user)s, %(date)s @ %(time)s drums guitars keyboards main page more more info review vocals Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-09-06 02:55+0300
PO-Revision-Date: 2015-02-04 20:49+0200
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: 
X-Generator: Poedit 1.6.5
 
                    <p>Ääriviivat on mielenkiintoinen pop/rock-yhtye Suomen musaskenessä ysärirokin sävyttämällä suomipopillaan. Helsinkiläisbändin musiikki toimii loistavasti niin levyllä kuin livenäkin vahvojen melodioiden ja kiinnostavien lyriikoiden ansiosta, mukaansatempaavaa meininkiä unohtamatta. Bändin taito ja aito rakkaus musiikkia kohtaan kuuluu ja näkyy.</p>
                    <p>Bändi sai alkunsa, kun työkaverit Mika Kaukoranta (kitara) ja Justin Goney (rummut) huomasivat jakavansa paljon musiikillisia vaikutteita 90-luvulta. Parivaljakko oli soitellut yhdessä jo jonkin aikaa, mutta kun he tapasivat laulaja Iitu Marjosalmen, projekti lähti kunnolla etenemään. "Pearl Jam -kovereiden soittamisessa on oma hohtonsa, ” Mika kertoo, ”mutta Iitun ääni ja energia veivät meidät uudelle tasolle".</p>
                    <p>Justinin ystävä Jane Koskimäki kutsuttiin tuomaan lisäulottuvuutta bändin soundiin koskettimilla, ja lopullinen kokoonpano syntyi, kun basisti Kimmo Vahteri liittyi joukkoon.</p>
                    <p>Ääriviivojen pop/rock-keitos tarjoaa yhtymäkohtia 90-luvun grungeen ja alternativeen, mutta vaikutteita haetaan myös laajemmalta skaalalta. Bändin sydäntä lähellä ovat mm. PMMP, Anna Puu ja Maija Vilkkumaa. Kaikki yhtyeen jäsenet - niin kliseiseltä, kuin se kuulostaakin - rakastavat musiikkia. Hyvän fiiliksen takaa myös se, että Ääriviivat soittaa juuri sellaista musiikkia, jota sen jäsenetkin haluavat kuunnella.</p>
                    <p>Ääriviivojen esikoisalbumi julkaistiin kesällä 2016. Levy koostuu kevyemmistä balladeista ja todellisesta rock-iloittelusta. Kappaleet on kirjoitettu yhdessä koko bändin voimin ja Iitun sanoin ”Olemme todella ylpeitä rakkaasta esikoisalbumistamme.” Levy esittelee erinomaisen läpileikkauksen Ääriviivojen musiikista, jossa kuuluu bändin täydellinen omistautuminen musiikille. Tähän asti yhtye on kiertänyt lähinnä Helsingin klubeja, mutta levyn julkaisun jälkeen bändi pyrkii laajentamaan keikkatarjontaansa myös pääkaupunkiseudun ulkopuolelle.</p>
                    <p>Löydät Ääriviivojen uusimmat uutiset ja keikkatiedot <a target="_blank" href="https://www.facebook.com/aariviivat">Facebookista</a></p>
                 
                    %(title)s (%(user)s - %(date)s, %(time)s)
                 
            <p>Ääriviivat on helsinkiläinen vuonna 2013 perustettu pop/rock-yhtye, jonka musiikki nojaa kiinnostaviin lyriikoihin, voimakkaisiin melodioihin sekä taidokkaaseen soittoon. Ääriviivat soittaa suurella sydämellä suomenkielistä poprockia, joka syntyy puhtaasta rakkaudesta musiikkiin.</p>
            <p>Pop. Voima. Rock. Rakkaus.</p>
         Info Info Info Huomio! Tule kuuntelemaan meitä %(date)s: %(venue)s, %(time)s! Huomio! Tule kuuntelemaan meitä tänä iltana: %(venue)s, %(time)s! Blogi Keikkojen sopiminen Yhteystiedot englanti Merkinnät tagilla suomi Keikkatiedustelut: <a href="mailto:band@aariviivat.fi">band@aariviivat.fi</a> tai <a target="_blank" href="https://www.facebook.com/aariviivat">Facebookissa</a>. Keikkatiedustelut: <a href="mailto:band@aariviivat.fi">band@aariviivat.fi</a> tai <a target="_blank" href="https://www.facebook.com/aariviivat">Facebookissa</a>. Haastattelut ja muut tiedustelut: <a href="mailto:band@aariviivat.fi">band@aariviivat.fi</a>. Sanat: %(credit)s Jäsenet Alasvetovalikon ikoni Musiikki Ei keikkoja juuri nyt, mutta suuntaamme pian tien päälle. Seuraathan meitä <a target="_blank" href="https://www.facebook.com/aariviivat">Facebookissa</a> niin saat ajantasaista tietoa. Oops! Sivua ei löydy Menneet keikat Lehdistö Uusimmat merkinnät Keikat Tagit Etsimääsi sivua ei nyt löydy, mutta onneksi voit kuitenkin nauttia musiikista: Biisit Tulevat keikat Olitpa sitten tapahtumanjärjestäjä tai bändi etsimässä kavereita keikalle, olemme kiinnostuneita yhteistyöstä. Olitpa sitten tapahtumanjärjestäjä tai bändi etsimässä kavereita keikalle, olemme kiinnostuneita yhteistyöstä. Mukana myös ^info/ ^yhteystiedot/ ^musikki/$ ^musikki/(?P<song_title>[\w\d-]+)/ ^keikat/ basso %(user)s - %(date)s, %(time)s rummut kitarat koskettimet etusivu lisää lisätietoa arvostelu laulu 