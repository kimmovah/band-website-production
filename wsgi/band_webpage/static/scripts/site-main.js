//'use strict';

// Site's main JS functionality is defined here...

function mobileDropdownMenu() {
  if ($('.small-dropdown-header').css('background-color') == 'rgb(233, 233, 233)') {
    $('.small-dropdown-menu:not(.hide-once)').addClass('hide-once').hide();
    $('.toggle:not(.bound)').addClass('bound').on('click', function () {
      $('.small-dropdown-menu').slideToggle(500);
    });
  } else {
    $('.small-dropdown-menu').show();
    $('.toggle').off('click').removeClass('bound');
  }
}

$(document).ready(function () {

  // Launch Foundation magic...
  $(document).foundation();

  // Add background image...
  $.backstretch('/static/images/backgrounds/004.jpg');

  // Call mobile dropdown menu, and attach resize event...
  mobileDropdownMenu();
  $(window).bind('resize', function () {
    clearTimeout(window.resizeEvt);
    window.resizeEvt = setTimeout(function () {
      mobileDropdownMenu();
    }, 250);
  });

  // Gracefully replace svg files with png if svg is not supported
  // TODO: test this
  if (!Modernizr.svg) {
    $("img[src*='svg']").attr('src', function () {
      return $(this).attr('src').replace('.svg', '.png');
    });
  }
});
