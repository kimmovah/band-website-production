from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

import apps.main.views

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^i18n/', include('robust_urls.urls')),

    url(r'^$', apps.main.views.HomePageView.as_view(), name='home'),

    url(_(r'^music/$'), apps.main.views.LyricsView.as_view(), name='lyrics'),
    url(_(r'^music/(?P<song_title>[\w\d-]+)/'), apps.main.views.LyricsView.as_view(), name='lyrics_by_title'),


    # url(_(r'^blog/$'), apps.main.views.BlogEntryView.as_view(), name='blog'),
    # url(_(r'^blog/(?P<blog_title>[\w\d-]+)/$'), apps.main.views.BlogEntryView.as_view(), name='blog_by_title'),
    # url(_(r'^blog/tags/(?P<tag>[\w\d-]+)/$'), apps.main.views.BlogTagsView.as_view(), name='blog_by_tags'),

    url(_(r'^shows/'), apps.main.views.ShowsView.as_view(), name='shows'),

    url(_(r'^about/'), apps.main.views.AboutPageView.as_view(), name='about'),

    url(_(r'^contact/'), apps.main.views.ContactPageView.as_view(), name='contact'),
)
