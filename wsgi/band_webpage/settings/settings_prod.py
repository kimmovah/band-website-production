import os


# Parse database configuration from $DATABASE_URL
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('PGDATABASE'),
        'USER': os.environ.get('OPENSHIFT_POSTGRESQL_DB_USERNAME'),
        'PASSWORD': os.environ.get('OPENSHIFT_POSTGRESQL_DB_PASSWORD'),
        'HOST': os.environ.get('OPENSHIFT_POSTGRESQL_DB_HOST'),
        'PORT': os.environ.get('OPENSHIFT_POSTGRESQL_DB_PORT'),
    }
}

# Static asset configuration
STATIC_ROOT = os.path.join(os.environ.get('OPENSHIFT_REPO_DIR'), 'wsgi', 'static')

SECRET_KEY = os.environ.get('OPENSHIFT_SECRET_TOKEN')

if os.environ.get('DEBUG') == 'True':
    DEBUG = True
else:
    DEBUG = False

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = [
    'bandwebsite-jgoney.rhcloud.com',  # Allow domain and subdomains
    '.aariviivat.fi',  # Also allow FQDN and subdomains
]
