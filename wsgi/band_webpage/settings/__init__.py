import socket

from settings import *

host = socket.gethostname()

DEBUG_HOSTS = (
    'jgoney-lnx',
    'Justin-PC',
    'Justins-MacBook-Pro-2.local',
    'justins-macbook.lan',
    'Justins-MBP',
    'Justins-MBP-2.lan',
    'Justins-MBP.lan',
    'Kimmos-MacBook-Air.local'
)

if host in DEBUG_HOSTS:
    from settings_dev import *
else:
    from settings_prod import *
